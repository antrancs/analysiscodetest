const MOVING_AVERAGE = 20;

document.addEventListener('DOMContentLoaded', function(event) {
  getDataSet('data/Oslo_STL.json', function(dataSet) {
    if (dataSet) {
      renderChart('STL', JSON.parse(dataSet));
    }
  });
  getDataSet('data/Stockholm_ABB.json', function(dataSet) {
    if (dataSet) {
      renderChart('ABB', JSON.parse(dataSet));
    }
  });
});

function renderChart(id, dataSet) {
  var data = dataSet.map(function(item, index, array) {
    return {
      x: new Date(item.date).toISOString(),
      y: item.last
    };
  });

  const bollingerbandsData = getBollingerBandsData(dataSet);
  const BOLLINGER_BAND_COLOR = '#FF5733';

  var context = document.querySelector('#' + id).getContext('2d');
  var stockChart = new Chart(context, {
    type: 'line',
    data: {
      datasets: [
        {
          label: id,
          fill: false,
          data: data,
          pointRadius: 0,
          borderColor: '#1a5ecc'
        },
        {
          label: 'Mid band',
          fill: false,
          data: bollingerbandsData.map(item => ({
            x: item.date,
            y: item.mid
          })),
          borderDash: [2, 2],
          pointRadius: 0,
          borderWidth: 2,
          borderColor: BOLLINGER_BAND_COLOR
        },
        {
          label: 'Upper band',
          fill: false,
          data: bollingerbandsData.map(item => ({
            x: item.date,
            y: item.uppper
          })),
          pointRadius: 0,
          borderColor: BOLLINGER_BAND_COLOR
        },
        {
          label: 'Lower band',
          fill: false,
          data: bollingerbandsData.map(item => ({
            x: item.date,
            y: item.lower
          })),
          pointRadius: 0,
          borderWidth: 1,
          borderColor: BOLLINGER_BAND_COLOR
        }
      ]
    },
    options: {
      responsive: false,
      scales: {
        xAxes: [
          {
            type: 'time',
            display: true
          }
        ]
      }
    }
  });
}

function getDataSet(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url);
  xhr.onload = function() {
    if (xhr.status == 200) {
      callback(xhr.responseText);
    } else {
      callback(null);
    }
  };
  xhr.send();
}

/// BOLLINGER BANDS IMPLEMENTATION
function getBollingerBandsData(dataSet) {
  const arr = [];
  for (let i = MOVING_AVERAGE - 1; i < dataSet.length; i++) {
    const movingArr = dataSet
      .slice(i - MOVING_AVERAGE + 1, i + 1)
      .map(item => item.last);

    const mean = getMean(movingArr);
    const standardDeviation = Math.sqrt(
      getMean(movingArr.map(item => Math.pow(item - mean, 2)))
    );

    arr.push({
      date: new Date(dataSet[i].date).toISOString(),
      mid: mean,
      uppper: mean + standardDeviation * 2,
      lower: mean - standardDeviation * 2
    });
  }

  return arr;
}

function getMean(arr) {
  if (arr.length === 0) {
    return 0;
  }

  const sum = arr.reduce((prev, cur) => prev + cur, 0);
  return sum / arr.length;
}
